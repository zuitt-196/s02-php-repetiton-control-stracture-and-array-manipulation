<?php
include_once("./code.php")
?>

<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S02: Repetition Control structure and array manipulation</title>
</head>

<body>
    <div>
        <h1>Divisible of Five</h1>
        <p><?php printDivisibleOfive(" ") ?></p>
    </div>
    <hr>

    <div>
        <h2>Array Manipulation</h2>
        <p><?php array_push($students, "John Smith") ?></p>
        <p><?php var_dump($students) ?></p>
        <pre><?php echo count($students) ?></pre>

        <p><?php array_push($students, "Jane Smith") ?></p>
        <p><?php var_dump($students) ?></p>
        <pre><?php echo count($students) ?></pre>


        <p><?php array_shift($students) ?></p>
        <p><?php var_dump($students) ?></p>
        <pre><?php echo count($students) ?></pre>

    </div>




</body>

</html>